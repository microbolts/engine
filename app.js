'use strict';

const https = require('https');
const path = require('path');
const fs = require('fs');
const LightningClient = require('lightning-client');
const express = require('express');
const sqlite3 = require('sqlite3');
const conf = require('./config.json')
const privateKey = fs.readFileSync('localhost.key');
const certificate = fs.readFileSync('localhost.cert');

const credentials = {
	key: privateKey,
	cert: certificate
};

const app = express();

const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(bodyParser.json());

// Lightning
const client = new LightningClient(conf.lightningdir, true);

// Sqlite
const openDB = function () {
	const dbPath = path.join(__dirname, 'db.sqlite');

	const openPromise = new Promise((resolve, reject) => {
		const db = new sqlite3.Database(dbPath, sqlite3.OPEN_READWRITE);

		db.on('error', err => {
			reject(err);
		});

		db.on('open', () => {
			resolve(db);
		});
	});

	const createPromise = function () {
		return new Promise((resolve, reject) => {
			const db = new sqlite3.Database(dbPath, sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE);

			db.on('error', err => {
				reject(err);
			});

			db.on('open', () => {
				const create_db = fs.readFileSync(path.join(__dirname, 'sql', 'create_db.sql')).toString();
				db.exec(create_db, err => {
					if (err) {
						return reject(err);
					}

					resolve(db);
				});
			});
		});
	};

	return openPromise
		.catch(_ => createPromise());
};

let db = null;
openDB()
	.then(_db => db = _db)
	.then(() => startServer())
	.catch(e => {
		console.log(e);
		process.exit(1);
	});

function startServer() {
	https.createServer(credentials, app).listen(31337, () => {
		console.log('Example app listening on port 31337!');
	});
}

const checkForPayment = function (token, decodedInvoice) {
	return new Promise((resolve, reject) => {
		const totalAmount = 0;
		console.log('checking for payment');

		const sql = ' \
		select b.public_key, b.key_limit, sum (a.amount) sigma, b.key_limit > sum (a.amount) within_limit \
		from ( \
			select * \
			from public_key_invoices \
			where date_insert >= datetime(\'now\',\'-1 day\') \
		) as a \
		left join public_key_authorizations as b \
		on a.public_key_ref = b.id \
		group by b.public_key, b.key_limit \
		having b.public_key = ?';

		const stmt = db.prepare(sql, token);
		stmt.all([], (err, rows) => {
			if (err) {
				return reject('SERVER_ERROR');
			}
			if (!rows[0]) {
				return reject('NO_PUBKEY_FOUND');
			}
			if (rows[0].within_limit === '1') {
				resolve();
			} else {
				return reject('OVERSPENT');
			}
		});
	});
};

// PAY
app.post('/pay', (req, res) => {
	res.set('Access-Control-Allow-Origin', '*'); // TODO: remove?

	const token = req.headers['x-browser-token'];
	const invoice = req.body.invoice;

	const run = async function () {
			const decodedInvoice = await client.decodepay(invoice);
			const paymentResult = await checkForPayment(token, decodedInvoice);

			client.pay(invoice)
			.then(info => {
				const public_key_authorizations_id = 'select id from public_key_authorizations where public_key = ?';
				const sql = 'INSERT INTO public_key_invoices (' + public_key_authorizations_id + ',amount) VALUES (?,?)';
				const stmt = db.prepare(sql, token, token, decodedInvoice.msatoshi);
				stmt.run(() => {
					res.send({
						error: null
					});
				});
			})
			.catch(err => {
				res.status(500).send({
					error: err
				});
				console.log(err);
			});
	};

	run()
		.catch(err => {
			console.log(err);

			res.status(500).send({
				error: err
			});
		});
});

process.on('SIGTERM', () => {
	db.close();
});
